---
layout: post
section: associations
title: Grupo de Usuarios de Linux de la Universidad Carlos III de Madrid
---

## Información

-   Universidad a la que está asociada: Universidad de Córdoba
-   Activa en la actualidad: Parcialmente

## Contacto

-   Sede física:
-   Página web: <https://www.gul.es/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/guluc3m>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
